# Memino Rasperino

Memino rasperino nasce per raccogliere tutti i miei meme riguardo il mondo raspberry che a cadenza giornaliera condivido in diversi gruppi.
Lo scopo dei meme è far risaltare diversi aspetti del Raspberry, dalle sue potenzialità (meme sul server), ai suoi difetti che lo rendono unico (la ram limitata, lo spreco di cpu o il surriscaldamento), tutto questo per guardare il proprio circuito e sorridergli quando con il pc ci rinchiudiamo a sfogare la nostra immaginazione arrivando in tutti i campi possibili.
Oltre a meme troverete consigli e guide sul mondo raspberry.

~ <https://t.me/raspinomemino>
